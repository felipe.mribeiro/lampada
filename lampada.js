const turnOn = document.getElementById ('turnOn') // document.getElementById busca pelo nome dado ao ID no documento index.html
const turnOff = document.getElementById ('turnOff')
const lamp = document.getElementById ('lamp')

function isLampBroken () {
    return lamp.src.indexOf ('quebrada') > -1  //faz varedura nas strings do src, traz verdadeiro ou falso 
}

function lampOn () {
    if ( !isLampBroken () ) {  //funcao dentro de uma funcao, verificando se ela nao estiver quebrada, troca a imagem
        lamp.src = "./img/ligada.jpg"   //função de nome LAMPoN que busca a imagem da lapmada ligada
}
}
function lampOff () {
    if ( !isLampBroken () ) {
        lamp.src = "./img/desligada.jpg"

}
}

function lampBroken () {
    lamp.src = "./img/quebrada.jpg"
}

turnOn.addEventListener ('click', lampOn) //evento que ao clicar, invoca a função lampOn e mostra a imagem buscada
turnOff.addEventListener ('click', lampOff)
lamp.addEventListener ('mouseover', lampOn)
lamp.addEventListener ('mouseleave', lampOff)
lamp.addEventListener ('dbclick', lampBroken)
